import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import * as serviceWorker from './serviceWorker';
import { BrowserRouter } from 'react-router-dom';
// import { ApolloProvider } from 'react-apollo';
import { ApolloClient } from 'apollo-boost';
import { createHttpLink } from 'apollo-link-http';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { setContext } from 'apollo-link-context';

// Set blank variables for apollo/graphql link
let httpLink;
let authLink;

initApp ("https://hismilecafe.myshopify.com/api/graphql", "d276ff99b6de2cbbf6f8b1660c5b118a", "AUD", "$", "CAFE");


// Function for building the app using dynamic params
function initApp (uri, accessToken, currencyCode, currencySymbol, store, adminAPI) { 
  httpLink = createHttpLink({
    uri
  });
  authLink = setContext((_, { headers }) => {
    return {
      headers: {
        'X-Shopify-Storefront-Access-Token': accessToken
      }
    }
  });
  const client = new ApolloClient({
    link: authLink.concat(httpLink),
    cache: new InMemoryCache()
  });
  ReactDOM.render((
    <BrowserRouter>
      <App 
        client={client}
        currencySymbol={currencySymbol}
        currencyCode={currencyCode}
        store={store}
        adminAPI={adminAPI}
      />
    </BrowserRouter>
  ), document.getElementById('root'))
}


// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
