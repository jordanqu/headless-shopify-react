export function CustomerLoginMutation(email, password) {
    return `
        mutation {
            customerAccessTokenCreate(input: {
                email: "${email}",
                password: "${password}"
            }) {
                userErrors {
                    field
                    message
                }
                customerAccessToken {
                    accessToken
                    expiresAt
                }
                customerUserErrors {
                    field
                    message
                }
            }
        }
    `;
}

export default { CustomerLoginMutation };
