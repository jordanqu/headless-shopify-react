export const CreateCheckoutMutation = `
    mutation {
        checkoutCreate(input: {}) {
            checkout {
                id
                webUrl
                subtotalPrice
                totalPrice
                lineItems(first: 10) {
                    edges {
                        node {
                            id
                            title
                            variant {
                                id
                                title
                                image {
                                src
                                }
                                price
                            }
                            quantity
                        }
                    }
                }
            }
        }
    }
`;
