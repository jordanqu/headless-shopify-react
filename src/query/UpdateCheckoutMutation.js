export function UpdateCheckoutMutation(checkoutID, variantID, quantity) {
    return (`
        mutation {
            checkoutLineItemsAdd(
                lineItems: [{ 
                    variantId: "${variantID}", 
                    quantity: ${quantity} 
                }], 
                checkoutId: "${checkoutID}",
            ) {
                checkout {
                    id
                    webUrl
                    totalTax
                    subtotalPrice
                    totalPrice
                        lineItems (first: 250) {
                            edges {
                                node {
                                id
                                title
                                variant {
                                    id
                                    title
                                    image {
                                        src
                                    }
                                    price
                                }
                                quantity
                            }
                        }
                    }
                }
            }
        }
    `)
}

export default { UpdateCheckoutMutation }