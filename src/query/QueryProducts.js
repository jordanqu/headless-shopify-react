export const QueryProducts = `
    query {
        shop {
            collectionByHandle(handle: "menu") {
                products(first: 20) {
                    edges {
                        node {
                        description
                        title
                        handle
                        id
                        images(first: 10) {
                            edges {
                                node {
                                    originalSrc
                                }
                            }
                        }
                        variants(first: 5) {
                            edges {
                                node {
                                        price
                                        compareAtPrice
                                        title
                                        id
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
`;
