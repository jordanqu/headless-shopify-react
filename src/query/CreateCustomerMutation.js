export function CreateCustomerMutation(email, password, firstName, lastName) {
    return `
        mutation {
            customerCreate(input: {
                email: "${email}",
                password: "${password}",
                firstName: "${firstName}",
                lastName: "${lastName}"
            }) {
                userErrors {
                    field
                    message
                }
                customer {
                    id
                }
            }
        }
    `;
}

export default { CreateCustomerMutation };
