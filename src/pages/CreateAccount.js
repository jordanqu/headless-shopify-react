import React, { Component } from "react";
import { NavLink } from "react-router-dom";

export class CreateAccount extends Component {
  state = {
    email: "",
    password: "",
    firstName: "",
    lastName: ""
  };
  componentDidMount = () => {
    document.title = "Create Account";
  };
  handleSubmit = event => {
    event.preventDefault();
    this.props.createAccount(
      this.state.email,
      this.state.password,
      this.state.firstName,
      this.state.lastName,
    );
  };
  handleEmail = event => {
    this.setState({
      email: event.target.value
    });
  };
  handlePassword = event => {
    this.setState({
      password: event.target.value
    });
  };
  handleFirstName = event => {
    this.setState({
      firstName: event.target.value
    });
  };
  handleLastName = event => {
    this.setState({
      lastName: event.target.value
    });
  };
  render() {
    let errors;
    if ( typeof this.props.createAccountData.customerCreate != "undefined" ) {
      errors = Object.entries(this.props.createAccountData.customerCreate.userErrors).map(([key, error]) => {
        return (
          <li key={key} className="mb-2 text-danger">
            {error.message}
          </li>
        )
      });
    }
    return (
      <div className="container">
        <div className="row justify-content-center">
          <div className="col-12 col-lg-4">
            <h1 className="my-4">Create Account</h1>
            { 
              typeof this.props.createAccountData.customerCreate != "undefined" && this.props.createAccountData.customerCreate.userErrors.length === 0
              ?
                <React.Fragment>
                  <p>Thanks for creating an account! <NavLink to={'/login'}>Login</NavLink> to get started.</p>
                  <p><NavLink to={'/login'}>Login</NavLink> to get started.</p>  
                </React.Fragment>
              :
              <React.Fragment>
                <form className="mb-2" onSubmit={this.handleSubmit}>
                  <div className="form-group">
                    <label>First Name:</label>
                    <input
                      className="form-control"
                      type="text"
                      onChange={this.handleFirstName}
                    />
                  </div>
                  <div className="form-group">
                    <label>Last Name:</label>
                    <input
                      className="form-control"
                      type="text"
                      onChange={this.handleLastName}
                    />
                  </div>
                  <div className="form-group">
                    <label>Email:</label>
                    <input
                      className="form-control"
                      type="text"
                      onChange={this.handleEmail}
                    />
                  </div>
                  <div className="form-group">
                    <label>Password:</label>
                    <input
                      className="form-control"
                      type="password"
                      onChange={this.handlePassword}
                    />
                  </div>
                  <button className="btn btn-primary form-control" type="submit">
                    Submit
                  </button>
                </form>
                <ul className="list-unstyled">
                  {errors}   
                </ul>
              </React.Fragment>
            }
          </div>
        </div>
      </div>
    );
  }
}
