import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import { CheckoutButton } from "../modules/CheckoutButton";
import { Increment } from "../modules/Increment";
import { Decrement } from "../modules/Decrement";

export class Cart extends Component {
  componentDidMount = () => {
    document.title = "Cart";
  };
  pageRefresh = () => {
    localStorage.clear();
    window.location.reload();
  };
  render() {
    const cartObj = this.props.cart;
    const cartReturned = Object.keys(cartObj).map(
      key => 
        cartObj[key].node.quantity > 0 ? (
          <tr key={key}>
            <td className="d-none d-lg-table-cell" style={{ width: "100px" }}>
              {<img className="mr-4 d-none d-lg-inline img-fluid" src={cartObj[key].node.variant.image.src} alt={cartObj[key].node.title} />}
            </td>
            <td className="py-4" valign="middle">
              {cartObj[key].node.title}
            </td>
            <td className="py-4" valign="middle">
              <div className="d-inline-block" style={{borderTop: "1px solid rgb(0, 0, 0)", borderBottom: "1px solid rgb(0, 0, 0)"}}>
                <Decrement 
                  decrement={this.props.handleUpdate}
                  id={cartObj[key].node.variant.id}
                  quantity={-1}>
                </Decrement>
                  <span className="mx-2">{cartObj[key].node.quantity}</span>
                <Increment 
                  increment={this.props.handleUpdate}
                  id={cartObj[key].node.variant.id}
                  quantity={1}>
                </Increment>  
              </div>
              
            </td>
            <td className="py-4 text-right" valign="middle">
              {cartObj[key].node.variant.price}
            </td>
          </tr>
        ) : null
    );
    return (
      <div className="container">
        <h1 className="my-4">Cart</h1>
        {
          this.props.itemCount > 0 ? (
            <div>
              <table className="table mb-2">
                <thead>
                  <tr>
                    <th className="d-none d-lg-table-cell"></th>
                    <th >
                      <strong>Product</strong>
                    </th>
                    <th>
                      <strong>Quantity</strong>
                    </th>
                    <th className="text-right">
                      <strong>Price</strong>
                    </th>
                  </tr>
                </thead>
                <tbody>{cartReturned}</tbody>
              </table>
              <div className="container">
                <div className="row">
                  <div className="col-12 text-right">
                    <h4 className="text-right mb-4" style={{lineHeight:"1.5"}}><strong>Total</strong><br/>{this.props.formatMoney(this.props.checkout.totalPrice)}</h4>
                  </div>
                </div>
              </div>
              <div className="container">
                <button onClick={this.pageRefresh} className="btn btn-secondary">Clear</button>
                <div className="row">
                  <div className="col-12 text-right">
                    <div>
                      <CheckoutButton url={this.props.checkout.webUrl}/>  
                    </div>
                  </div>
                </div>
              </div>
            </div>
          )
          :
          <p>There's nothing in your cart! <NavLink to={'/products'}>Let's change that!</NavLink></p>
        }
      </div>
    );
  }
}
