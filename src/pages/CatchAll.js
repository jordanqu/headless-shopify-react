import React, { Component } from "react";

export class CatchAll extends Component {
  componentDidMount = () => {
    document.title = "404 Not found";
  };
  render() {
    return (
      <div className="container">
        <h1 className="my-4">404</h1>
        <h2>You're lost my friend!</h2>
      </div>
    );
  }
}
