import React, { Component } from 'react';

export class Contact extends Component {
    componentDidMount = () => {
        document.title = "Contact";
      }
    render() {
        return (
            <div className="container">
                <h1 className="my-4">Contact</h1>    
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p> 
                <p>PH: <a href="#test">01010 10010</a></p>
                <p>Email: <a href="#test">headless@headless.com</a></p>
            </div>
        );
    }
}