import React, { Component } from "react";
import { NavLink } from "react-router-dom";

export class Home extends Component {
  componentDidMount = () => {
    document.title = "Headless Shopify App";
  };
  render() {
    return (
      <>
        <div className="jumbotron hero mb-100">
          <div className="container">
            <div className="row justify-content-center">
              <div className="col-12 text-center">
                <h1 className="display-4 mb-4">Example <br/><u>Headless</u> store</h1>
                <p className="lead">
                  <NavLink className="btn btn-primary btn-lg" to={"/products"} role="button">Shop</NavLink>
                </p>
              </div>
            </div>
          </div>
        </div>
        <div className="container mb-100">
          <div className="row align-items-center">
            <div className="col-lg-5 pr-lg-5">
            <p className="lead mb-3">COMMUNITY SPOTLIGHT</p>
            <h2 className="mb-3">“It’s a fun and fast<br/> way to build a headless”</h2>
            <p className="mb-3">— Ashton, 23 years old, Sydney<br/>
            — Customer since 2016</p>
            <p className="mb-3"><a className="btn btn-primary btn btn-lg" href="#">Read Ashton's Story &#10230;</a></p>
            </div>
            <div className="col-lg-7">
              <img className="img-fluid" src="https://dummyimage.com/1200x800/e9ecef/292929.jpg" alt="Story lead"></img>
            </div>
          </div>
        </div>
        <div className="container mb-100">
          <div className="row flex-wrap-reverse">
            <div className="col-lg-5">
              <img className="img-fluid" src="https://dummyimage.com/800x1200/e9ecef/292929.jpg" alt="Story lead"></img>
            </div>
            <div className="col-lg-7 pl-lg-5">
              <h2 className="mb-3 h1"><b>The Way<br/> to Build</b></h2>
              <p className="mb-3">Designed to lorem ipsum dolar amet.</p>
              <p className="mb-3"><a className="btn btn-primary btn btn-lg" href="#">LEARN MORE ABOUT THE KIT &#10230;</a></p>
            </div>
          </div>
        </div>
        <div className="container mb-100">
          <div className="row align-items-center">
            <div className="col-lg-5 pr-lg-5">
            <h2 className="mb-3">What people are saying about us</h2>
            <p className="mb-3 h3">&#9733; &#9733; &#9733; &#9733; &#9733;</p>
            <p className="mb-3">5 out of 5 stars  |  11,111 Reviews</p>
            <p className="mb-3"><a className="btn btn-primary btn btn-lg" href="#">Read our reviews &#10230;</a></p>
            </div>
            <div className="col-lg-7">
              <img className="img-fluid" src="https://dummyimage.com/1200x800/e9ecef/292929.jpg" alt="Story lead"></img>
            </div>
          </div>
        </div>
        <div className="container mb-100">
          <div className="row flex-wrap-reverse">
            <div className="col-lg-5">
              <img className="img-fluid" src="https://dummyimage.com/800x1200/e9ecef/292929.jpg" alt="Story lead"></img>
            </div>
            <div className="col-lg-7 pl-lg-5">
              <h2 className="mb-3 h1"><b>Our Product Plans</b></h2>
              <p className="mb-3">Have all of your favourite products delivered to your front door at a frequency of your choice with our Product Plans. Simply choose from our most popular products and build a plan that’s right for you. </p>
              <p className="mb-3"><a className="btn btn-primary btn btn-lg" href="#">Choose a Plan &#10230;</a></p>
            </div>
          </div>
        </div>
        <div className="container mb-100">
          <div className="row align-items-center mb-5">
            <div className="col-lg-8">
              <h2>Our community</h2>
            </div>
            <div className="col-lg-4"> 
              <a className="btn btn-primary btn btn-lg" href="#">Head to the hub &#10230;</a>
            </div>
          </div>
          <div className="row">
            <div className="col-lg-4 mb-5 mb-lg-0">
              <img className="img-fluid mb-3" src="https://dummyimage.com/800x800/e9ecef/292929.jpg" alt=""></img>
              <p className="mb-3 lead">ABOUT US</p>
              <h3 className="mb-3">How we got started</h3>
              <a href="#" className="anchor">READ OUR STORY &#10230;</a>
            </div>
            <div className="col-lg-4 mb-5 mb-lg-0">
              <img className="img-fluid mb-3" src="https://dummyimage.com/800x800/e9ecef/292929.jpg" alt=""></img>
              <p className="mb-3 lead">SOCIAL FEEDS</p>
              <h3 className="mb-3">Our favourite posts</h3>
              <a href="#" className="anchor">SCOPE THE SOCIALS &#10230;</a>
            </div>
            <div className="col-lg-4 mb-5 mb-lg-0">
              <img className="img-fluid mb-3" src="https://dummyimage.com/800x800/e9ecef/292929.jpg" alt=""></img>
              <p className="mb-3 lead">HUB STORE</p>
              <h3 className="mb-3">Exclusive products</h3>
              <a href="#" className="anchor">COP THE LATEST DROPS &#10230;</a>
            </div>
          </div>
        </div>
      </>
    )
  }
}
