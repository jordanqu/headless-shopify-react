import React, { Component } from "react";
import { NavLink } from "react-router-dom";

export class Products extends Component {
  componentDidMount = () => {
    document.title = "Products";
  };
  render() {
    return (
      <div>
        <div className="container" id="product-grid">
          <h1 className="my-4">Products</h1>
          <div className="row">
            { 
              Object.entries(this.props.products).map(([key, product]) => {
                return (
                  !product.handle.includes("subscription") ?
                    <div key={key} className="col-lg-4 mb-4">
                      <NavLink exact={true} to={product.handle} className="no-decoration">
                        <div className="p-4" style={{border:"1px solid #efefef"}}>
                          <img className="img-fluid mb-4"  src={product.images[0].node.originalSrc} alt={product.title} />
                          <h2 className="h5 mb-2 text-dark"><strong>{product.title}</strong></h2>
                          <p className="text-dark mb-0" style={{opacity:"0.6"}}>{this.props.formatMoney(product.price)}</p>  
                        </div>
                      </NavLink>
                    </div>
                  : null
                )
              })
            }
          </div>
        </div>
      </div>
    );
  }
}
