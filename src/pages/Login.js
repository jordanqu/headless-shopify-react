import React, { Component } from "react";
import { Redirect } from 'react-router-dom';

export class Login extends Component {
  state = {
    email: "",
    password: ""
  };
  componentDidMount = () => {
    document.title = "Login";
  };
  handlePassword = event => {
    this.setState({
      password: event.target.value
    });
  };
  handleEmail = event => {
    this.setState({
      email: event.target.value
    });
  };
  handleSubmit = event => {
    event.preventDefault();
    this.props.customerLogin(
      this.state.email,
      this.state.password
    );
  };
  render() {
    let message;
    if ( this.props.customerLoginData.customerAccessTokenCreate && typeof this.props.customerLoginData.customerAccessTokenCreate.userErrors !== "undefined" && this.props.customerLoginData.customerAccessTokenCreate.userErrors.length > 0 ) {
      message = <li className="mb-2 text-danger">Incorrect email or password</li>
    }
    else if ( this.props.customerLoginData.customerAccessTokenCreate && this.props.customerLoginData.customerAccessTokenCreate.accessToken !== "" ) {
      message = <Redirect to='/account'/>;
    }
    
    return (
      <div className="container">
        <div className="row justify-content-center">
            <div className="col-12 col-lg-4">
                <h1 className="my-4">Login</h1>
                <form className="mb-2" onSubmit={this.handleSubmit}>
                    <div className="form-group">
                        <label>Email:</label>
                        <input className="form-control" type="text" onChange={this.handleEmail} />
                    </div>
                    <div className="form-group">
                        <label>Password:</label>
                        <input className="form-control" type="password" onChange={this.handlePassword} />
                    </div>
                    <button className="btn btn-primary form-control" type="submit">Submit</button>
                </form>
                <ul className="list-unstyled">
                  {message}
                </ul>
            </div>
        </div>
      </div>
    );
  }
}
