import { NavLink } from "react-router-dom";
import React, { Component } from "react";


export class Footer extends Component {
  render() {
    const productLinks = Object.entries(this.props.products).map(([key, product]) => {
        return (
          <li key={product + key} className="mb-2">
              <NavLink
              className="text-dark small" 
              to={product.handle}
              >
              {product.title}
              </NavLink>
          </li>
        );
      }
    );
    return (
      <React.Fragment>
        <footer className="py-4">
          <div className="container my-4">
            <div className="row">
              <div className="col-lg-4 text-center text-lg-left">
                <h4>Sitemap</h4>
                <ul className="list-unstyled mb-0">
                  <li className="mb-2">
                    <NavLink className="text-dark small" to="/">
                      Home
                    </NavLink>
                  </li>
                  <li className="mb-2">
                    <NavLink className="text-dark small" to="/products">
                      Products
                    </NavLink>
                  </li>
                  <li className="mb-2">
                    <NavLink className="text-dark small" to="/contact">
                      Contact
                    </NavLink>
                  </li>
                  <li className="mb-2">
                    <NavLink className="text-dark small" to="/about">
                      About
                    </NavLink>
                  </li>
                  <li className="mb-2">
                    <NavLink className="text-dark small" to="/cart">
                      Cart
                    </NavLink>
                  </li>
                  <li className="mb-2">
                    <NavLink className="text-dark small" to="/login">
                      Login
                    </NavLink>
                  </li>
                  <li className="mb-2">
                    <NavLink className="text-dark small" to="/create-account">
                      Create Account
                    </NavLink>
                  </li>
                </ul>
              </div>
              <div className="col-lg-4 text-center my-4 my-lg-0 py-4 py-lg-0">
                <NavLink to="/">
                  <img
                    style={{ maxWidth: "200px" }}
                    src="https://cdn.shopify.com/s/files/1/0503/7306/6914/files/React-logo-1.png?v=1606798121"
                    alt=""
                  />
                </NavLink>
              </div>
              <div className="col-lg-4 text-center text-lg-right">
                <h4>Products</h4>
                <ul className="list-unstyled mb-0">
                    {productLinks}
                </ul>
              </div>
            </div>
          </div>
        </footer>
        <p className="py-2 m-0 bg-dark text-light text-center small">
          <strong>This is a concept React / React Router / Apollo / GraphQL / Shopify App</strong>
        </p>
      </React.Fragment>
    );
  }
}
