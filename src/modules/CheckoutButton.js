import React, { Component } from "react";

export class CheckoutButton extends Component {
    render() {
        return (
            <a className="btn btn-primary btn-lg" href={this.props.url}>Checkout</a>    
        );
    }
}