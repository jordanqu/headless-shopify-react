import React, { Component } from "react";

export class Decrement extends Component {
    render() {
        return (
            <button className="quantity-btn" onClick={() => this.props.decrement(this.props.id, this.props.quantity)}>-</button>
        );
    }
}

export default Decrement;