import { Route, Switch } from 'react-router-dom';
import React, { Component } from 'react';

import { Contact } from '../pages/Contact.js';
import { Home } from '../pages/Home';
import { About } from '../pages/About';
import { Products } from '../pages/Products';
import { CatchAll } from '../pages/CatchAll';
import { Product } from './Product';
import { Cart } from '../pages/Cart';
import { Login } from '../pages/Login';
import { CreateAccount } from '../pages/CreateAccount';
import { Account } from '../pages/Account';

export class Main extends Component { 
    render() {

        const { 
            handleUpdate, 
            products, 
            onSubscriptionUpdate, 
            itemCount, 
            formatMoney, 
            cart, 
            checkout, 
            createAccount, 
            createAccountData, 
            customerLogin, 
            customerLoginData,
        } = this.props;

        return (
            <main className="">
                <Switch>
                    <Route exact={true} path="/" component={Home} />

                    <Route path="/contact" component={Contact} />

                    <Route path="/about" component={About} />

                    <Route exact={true} path='/products' render={(props) => (
                        <Products {...props} products={products} formatMoney={formatMoney} />
                    )}/>

                    {
                        Object.entries(this.props.products).map(([key, product]) => {
                            return (
                                <Route key={product + key} exact={true} path={product.handle} render={(props) => (
                                    <Product {...props} products={products} formatMoney={formatMoney} handleUpdate={handleUpdate} onSubscriptionUpdate={onSubscriptionUpdate} />
                                )}/>
                            );
                        })
                    }

                    <Route exact={true} path='/cart' render={(props) => (
                        <Cart {...props} itemCount={itemCount} handleUpdate={handleUpdate} formatMoney={formatMoney} checkout={checkout} cart={cart} />
                    )}/>

                    <Route exact={true} path='/login' render={(props) => (
                        <Login {...props} customerLogin={customerLogin} customerLoginData={customerLoginData} />
                    )}/>

                    <Route exact={true} path='/create-account' render={(props) => (
                        <CreateAccount {...props} createAccount={createAccount} createAccountData={createAccountData} />
                    )}/>

                    <Route exact={true} path='/account' render={(props) => (
                        <Account {...props} createAccount={createAccount} createAccountData={createAccountData} />
                    )}/>
                    
                    <Route component={CatchAll} /> {/* 404 page - must come last */}
                    
                </Switch>
            </main>
        );
    }
}
