import { NavLink } from "react-router-dom";
import React, { Component } from "react";
import cartIcon from "../img/cart_icon.png";

import australiaIcon from "../img/australia.svg";
import usIcon from "../img/united-states.svg";
import ukIcon from "../img/united-kingdom.svg";
import euIcon from "../img/european-union.svg";
import worldIcon from "../img/worldwide.svg";
import jpIcon from "../img/japan.svg";

export class Header extends Component {
  state = {
    navToggled: false
  }
  toggleNav = (e) => {
    this.setState(currentState => {
      return {
        navToggled: !currentState.navToggled
      };
    });
  }
  closeCart = () => {
    this.props.closeCart();
  }
  render() {
    let icon;
    if ( this.props.store === "AU" ) {
      icon = australiaIcon;
    }
    else if ( this.props.store === "US" ) {
      icon = usIcon;
    }
    else if ( this.props.store === "UK" ) {
      icon = ukIcon;
    }
    else if ( this.props.store === "EU" ) {
      icon = euIcon;
    }
    else if ( this.props.store === "JP" ) {
      icon = jpIcon;
    }
    else {
      icon = worldIcon;
    }
    return (
      <React.Fragment>
        <nav className="navbar navbar-expand-lg navbar-light bg-light py-3">
          <div className="container" style={{position:"relative"}}>
            <button
              className={this.state.navToggled ? "toggled navbar-toggler px-0" : "navbar-toggler px-0"}
              type="button"
              onClick={this.toggleNav}
            >
              <span></span>
              <span></span>
              <span></span>
            </button>
            <NavLink className="navbar-brand m-auto" to="/">
              <img
                height="35"
                alt=""
                src="https://cdn.shopify.com/s/files/1/0503/7306/6914/files/React-logo-1.png?v=1606798121"
              />
            </NavLink>
            <span className="mobile-flag flag d-lg-none">
              <span onClick={this.props.toggleRegionSelect}>
                <img src={icon} alt="Country Icon"></img>
              </span>
              <ul className="list-unstyled country-select" style={{ display: this.props.regionSelect === true ? "flex" : "none" }}>
                <li><a href="/?rdr=au"><img src={australiaIcon} alt="AUS Icon"></img></a></li>
                <li><a href="/?rdr=us"><img src={usIcon} alt="US Icon"></img></a></li>
                <li><a href="/?rdr=uk"><img src={ukIcon} alt="UK Icon"></img></a></li>
                <li><a href="/?rdr=eu"><img src={euIcon} alt="EU Icon"></img></a></li>
                <li><a href="/?rdr=jp"><img src={jpIcon} alt="JP Icon"></img></a></li>
                <li><a href="/?rdr=int"><img src={worldIcon} alt="INT Icon"></img></a></li>
              </ul>
            </span>
            <NavLink className="nav-link d-inline d-lg-none" to="/cart" style={{position:"relative"}}>
              <span className="badge badge-pill badge-primary" style={{ position: "absolute", color:"#fff", backgroundColor:"#007bff", zIndex:"1", top:"-5px" }}>
                {this.props.itemCount.toString()}
              </span>
              <img height="20" src={cartIcon} alt="Shopping Cart" style={{ position:'absolute', left:'4px', zIndex:'0', bottom:'-5px' }} />
            </NavLink>
            <div className={this.state.navToggled ? "navbar-collapse" : "collapse navbar-collapse"} id="navbarNav">
              <ul className="navbar-nav ml-auto">
                <li className="nav-item px-lg-4">
                  <NavLink exact={true} className="nav-link" to="/">
                    Home
                  </NavLink>
                </li>
                <li className="nav-item px-lg-4">
                  <NavLink className="nav-link" to="/products">
                    Products
                  </NavLink>
                </li>
                <li className="nav-item px-lg-4">
                  <NavLink className="nav-link" to="/contact">
                    Contact
                  </NavLink>
                </li>
                <li className="nav-item px-lg-4">
                  <NavLink className="nav-link" to="/about">
                    About
                  </NavLink>
                </li>
                <li className="nav-item pl-4">
                  <NavLink onClick={this.closeCart} className="nav-link d-none d-lg-block" to="/cart" style={{position:"relative"}}>
                    <span id="cartBubble" className="badge badge-pill badge-primary" style={{ position: "absolute", color:"#fff", backgroundColor:"#007bff", zIndex:"1", top:"2px" }}>
                      {this.props.itemCount.toString()}
                    </span>
                    <img height="20" src={cartIcon} alt="Shopping Cart" style={{ position:'absolute', left:'0', zIndex:'0', bottom:'-12px' }} />
                  </NavLink>
                </li>
                <li className="nav-item pl-4 ml-4 flag d-none d-lg-block">
                  <span onClick={this.props.toggleRegionSelect}>
                    <img src={icon} alt="Country Icon"></img>
                  </span>
                  <ul className="list-unstyled country-select" style={{ display: this.props.regionSelect === true ? "flex" : "none" }}>
                    <li><a href="/?rdr=au"><img src={australiaIcon} alt="AUS Icon"></img></a></li>
                    <li><a href="/?rdr=us"><img src={usIcon} alt="US Icon"></img></a></li>
                    <li><a href="/?rdr=uk"><img src={ukIcon} alt="UK Icon"></img></a></li>
                    <li><a href="/?rdr=eu"><img src={euIcon} alt="EU Icon"></img></a></li>
                    <li><a href="/?rdr=jp"><img src={jpIcon} alt="JP Icon"></img></a></li>
                    <li><a href="/?rdr=int"><img src={worldIcon} alt="INT Icon"></img></a></li>
                  </ul>
                </li>
              </ul>
            </div>
          </div>
        </nav>
        <p className="py-2 m-0 bg-dark text-light text-center small"><strong>*EXAMPLE* 100% RESULTS GUARANTEED *EXAMPLE*</strong></p>  
      </React.Fragment>
    );
  }
}
