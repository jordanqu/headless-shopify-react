import React, { Component } from "react";

export class Increment extends Component {
    handleClick = () => {
        this.props.increment(this.props.id, this.props.quantity);
    }
    render() {
        return (
            <button className="quantity-btn" onClick={this.handleClick}>+</button>
        );    
    }
}

export default Increment;