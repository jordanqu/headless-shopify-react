import { NavLink } from "react-router-dom";
import React, { Component } from "react";
import { CheckoutButton } from "../modules/CheckoutButton";
import { Increment } from "./Increment";
import { Decrement } from "./Decrement";


export class SlideOutCart extends Component {
  render() {
    const cartObj = this.props.cart;
    const cartReturned = Object.keys(cartObj).map(
      key => 
        cartObj[key].node.quantity > 0 ? (
          <tr key={key}>
            <td className="pl-4" style={{ width: "80px" }}>
              {
                <img className="img-fluid" src={cartObj[key].node.variant.image.src} alt={cartObj[key].node.title} /> 
              }  
            </td>
            <td className="py-4" valign="middle">
              <div className="mb-3">{cartObj[key].node.title}</div>
              <div>
                <div className="d-inline-block" style={{borderTop: "1px solid #000", borderBottom: "1px solid #000"}}>
                  <Decrement 
                    decrement={this.props.handleUpdate}
                    id={cartObj[key].node.variant.id}
                    quantity={-1}>
                  </Decrement>
                    <span className="mx-2">{cartObj[key].node.quantity}</span>
                  <Increment 
                    increment={this.props.handleUpdate}
                    id={cartObj[key].node.variant.id}
                    quantity={1}>
                  </Increment>  
                </div>
              </div>
            </td>
            <td className="py-4 pr-4 text-right" valign="top">
              {
                cartObj[key].node.variant.price
              }
            </td>
          </tr>
        ) : null
    );
    return (
      <div className="cart-slider" style={{ marginRight: this.props.cartOpen === true ? "0" : "-100%" }}>
        <div style={{ backgroundColor:"#fff", borderBottom: "1px solid #e2e2e2" }}>
          <span className="close-cart-slider" onClick={this.props.closeCart}>&times;</span>
              <h3 className="py-4 ml-4">Your Cart</h3>
              {
              this.props.itemCount > 0 ? (
                <div>
                    <table className="table mb-2">
                      <thead>
                        <tr>
                          <th className="pl-4">
                            <strong>Product</strong>
                          </th>
                          <th style={{minWidth:"120px"}}>
                            <strong>Quantity</strong>
                          </th>
                          <th className="text-right pr-4">
                            <strong>Price</strong>
                          </th>
                        </tr>
                      </thead>
                      <tbody>{cartReturned}</tbody>
                    </table>
                    <div className="total container p-4">
                      <div className="row">
                        <div className="col-6 text-left">
                            <p className="text-secondary" style={{lineHeight:"1.5"}}>Estimated Total</p>
                        </div>
                        <div className="col-6 text-right">
                            <p>{this.props.formatMoney(this.props.checkout.totalPrice)}</p>
                        </div>
                        <div className="col-12 text-right">
                          <div>
                            <CheckoutButton url={this.props.checkout.webUrl}/>  
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                )
              :
              <p>There's nothing in your cart! <NavLink to={'/products'}>Let's change that!</NavLink></p>
              }  
            </div>
        </div>
    );
  }
}
