import React, { Component } from "react";
import Slider from "react-slick";
import _ from "lodash";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

export class Product extends Component {
  state = {
    hideSubscriptionUI: true,
    quantitySelected: 1,
    subscription: {
      quantitySelected: 1,
      frequency: 1
    }
  };
  componentDidMount = () => {
    const url = this.props.match.path;
    const product = _.find(this.props.products, function(product) { return product.handle === url; });
    document.title = product.title;
  };
  handlehideSubscriptionUI = e => {
    this.setState(currentState => {
      return {
        hideSubscriptionUI: !currentState.hideSubscriptionUI
      };
    });
  };
  handleChange = e => {
    this.setState({
      quantitySelected: e.target.value
    });
  };
  handleQuantityChange = e => {
    let subscriptionClone = {...this.state.subscription};
    subscriptionClone.quantitySelected = e.target.value;
    this.setState({
      subscription: subscriptionClone
    });
  };
  handleFrequencyChange = e => {
    let subscriptionClone = {...this.state.subscription};
    subscriptionClone.frequency = e.target.value;
    this.setState({
      subscription: subscriptionClone
    });
  };
  render() {
    const url = this.props.match.path;
    const product = _.find(this.props.products, function(product) { return product.handle === url; });
    const settings = {
      dots: true,
      infinite: true,
      arrows: true,
      autoplay: true,
      pauseOnHover: true,
      autoplaySpeed: 6000,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1
    };
    return (
      <div>
        <div className="container py-4 my-4">
          <a
            className="mb-4 d-block text-dark"
            href="#back"
            onClick={() => this.props.history.goBack()}
          >
            &larr; Back
          </a>
          <div className="row justify-content-around">
            <div className="col-lg-7 mb-4 mb-lg-0 pb-4 pb-lg-0">
              <Slider {...settings}>
                {
                  product.images.map((img, key) => {
                  return (
                    <div key={key}>
                      <img alt="" className="img-fluid" src={img.node.originalSrc} />
                    </div>
                  );
                  })
                }
              </Slider>
            </div>
            <div className="col-lg-5">
              <div className="container">
                <h1 className="h2 mb-4">{product.title}</h1>
                <p className="mb-4">{product.description}</p>
                <p className="h4 mb-4" style={{ display: this.state.hideSubscriptionUI === false ? "none" : "block" }}>{this.props.formatMoney(product.price)}</p>
                <p className="h4 mb-4" style={{ display: this.state.hideSubscriptionUI === false ? "block" : "none" }}><span style={{textDecoration:"line-through",color:"#efb0bb"}}>{this.props.formatMoney(product.price)}</span> <span>{this.props.formatMoney(((product.price)-(product.price/3)).toFixed(2))}</span></p>
                <div
                  className="row mb-4"
                  style={{
                    display:
                      this.state.hideSubscriptionUI === false ? "none" : "flex"
                  }}
                >
                  <div className="col-9">
                    <button
                      className={
                        product.quantity === 5
                          ? "disabled btn btn-primary btn-lg w-100"
                          : "btn btn-primary btn-lg w-100"
                      }
                      onClick={ () => this.props.handleUpdate(product.id, this.state.quantitySelected) }
                    >
                      Add to Cart
                    </button>
                  </div>
                  <div className="col-3">
                    <label className="sr-only" htmlFor="quantity">
                      Quantity:{" "}
                    </label>
                    <select onChange={this.handleChange} className="form-control h-100" name="quantity">
                      <option disabled>Please select one</option>
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4">4</option>
                      <option value="5">5</option>
                    </select>
                  </div>
                  <div className="col-9 text-center">
                    <small className="text-danger">
                      {product.quantity === 5 ? "Max 5" : null}
                    </small>
                  </div>
                </div>
                <button style={{display:this.state.hideSubscriptionUI === false ? "none" : "block" }} onClick={this.handlehideSubscriptionUI} className="btn btn-secondary btn-lg w-100">
                  Create a subscription
                </button>
                <div
                  className="pt-4"
                  style={{
                    display:
                      this.state.hideSubscriptionUI === true ? "none" : "block",
                    borderTop: "1px solid #efefef"
                  }}
                >
                  <label className="mb-1" htmlFor="frequency">
                    Frequency:{" "}
                  </label>
                  <select
                    onChange={this.handleFrequencyChange}
                    className="mb-2 form-control"
                    name="frequency"
                  >
                    <option disabled>Please select one</option>
                    <option value="1">Monthly</option>
                    <option value="2">Every 2 months</option>
                    <option value="3">Every 3 months</option>
                  </select>
                  <label className="mb-1" htmlFor="subscription-quantity">
                    Quantity:{" "}
                  </label>
                  <select
                    className="mb-4 form-control"
                    name="subscription-quantity"
                    onChange={this.handleQuantityChange}
                  >
                    <option disabled>Please select one</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                  </select>
                  <button style={{ width: "100%" }} className="btn btn-secondary btn-lg mb-1"
                    onClick={() =>
                      this.props.onSubscriptionUpdate(
                        "mouthwash",
                        this.state.subscription.quantitySelected,
                        this.state.subscription.frequency
                      )
                    }>Add subscription</button>
                  <p className="text-center">
                    <a href="#cancel" className="text-dark" onClick={this.handlehideSubscriptionUI}>Cancel</a>
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="container py-4">
          <hr className="my-4"></hr>
        </div>
        <div className="container mb-4 pb-4">
          <div className="row">
            <div className="col-lg-4">
              <img className="img-fluid" src={product.images[0].node.originalSrc} alt={product.title}></img>
            </div>
            <div className="col-lg-8">
              <h4>Info about {product.title}</h4>
              <p>{product.description} Duis aute irure dolor
              in reprehenderit in voluptate velit esse cillum dolore eu fugiat
              nulla pariatur. Excepteur sint occaecat cupidatat non proident,
              sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
              <button
                className={
                  product.quantity === 5
                    ? "disabled btn btn-primary btn-lg"
                    : "btn btn-primary btn-lg"
                }
                onClick={() =>
                  this.props.handleUpdate(product.id, this.state.quantitySelected)
                }
              >
                Add to Cart - {this.props.formatMoney(product.price)}
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
