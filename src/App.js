import React, { Component } from "react";
import { Header } from "./modules/Header";
import { Main } from "./modules/Main";
import { Footer } from "./modules/Footer";
import { SlideOutCart } from "./modules/SlideOutCart";
import gql from "graphql-tag";

// ============= IMPORT CLIENT GRAPHQL QUERIES/MUTATIONS ==================
import { CreateCheckoutMutation } from "./query/CreateCheckoutMutation";
import { QueryProducts } from "./query/QueryProducts";
import { UpdateCheckoutMutation } from "./query/UpdateCheckoutMutation";
import { CreateCustomerMutation } from "./query/CreateCustomerMutation";
import { CustomerLoginMutation } from "./query/CustomerLoginMutation";

// ============= IMPORT GLOBAL CSS ==================
import "bootstrap/dist/css/bootstrap.min.css";
import "./sass/App.scss";

class App extends Component {
  state = {
    products: {},
    subscriptionProducts: {},
    itemCount: 0,
    checkout: {},
    cart: {},
    cartOpen: false,
    createAccountData: {},
    customerLoginData: {},
    regionSelect: false,
    articles:{}
  };

  componentWillMount = () => {

    const client = this.props.client;

    // Fetch products
    client
      .query({
        query: gql`${QueryProducts}`
      })
      .then(res => {
        console.log(res);
        const productArr = res.data.shop.collectionByHandle.products.edges;
        let products = {};
        productArr.forEach(e => {
          products[e.node.title] = {
            title:        e.node.title,
            handle:       `/products/${e.node.handle}`,
            images:       e.node.images.edges,
            price:        e.node.variants.edges[0].node.price,
            description:  e.node.description,
            id:           e.node.variants.edges[0].node.id
          };
        });
        this.setState({
          products
        });
      });

    // Create checkout object
    client.mutate({
      mutation: gql`${CreateCheckoutMutation}`
    })
    .then(res => {
      this.setState({
        checkout: res.data.checkoutCreate.checkout
      })
    })

  }


  formatMoney = value => {
    return `${this.props.currencySymbol}${value} ${this.props.currencyCode}`;
  }


  handleUpdate = (variantID, quantity) => {
    document.getElementById("cartBubble").classList.add("pop");
    const client = this.props.client;
    client
      .mutate({
        mutation: gql`${UpdateCheckoutMutation(this.state.checkout.id, variantID, quantity)}`
      })
      .then(response => {
        document.getElementById("cartBubble").classList.remove("pop");
        var cart = response.data.checkoutLineItemsAdd.checkout.lineItems.edges,
          itemCount = cart.length;
        this.setState({
          itemCount,
          cart,
          checkout: response.data.checkoutLineItemsAdd.checkout
        })
        this.openCart()
      })
  }


  openCart = () => {
    if ( !window.location.pathname.includes("cart") ) {
      this.setState({
        cartOpen: true
      })  
    }
  }


  toggleRegionSelect = () => {
    this.setState(currentState => {
      return {
        regionSelect: !currentState.regionSelect
      }
    })
  }


  closeCart = () => {
    this.setState({
      cartOpen: false
    })
  }


  handleSubscriptionUpdate = (e, quantity, frequency) => {
    alert("This feature is not implemented, but if it were, it should add a subscription variant of the same product to the cart.");
  };


  createAccount = (email, password, firstName, lastName) => {
    const client = this.props.client;
    client.mutate({mutation: gql`${CreateCustomerMutation(email, password, firstName, lastName)}`})
    .then(response => {
      this.setState({
        createAccountData: response.data
      })
    });
  }


  customerLogin = (email, password) => {
    const client = this.props.client;
    client.mutate({mutation: gql`${CustomerLoginMutation(email, password)}`})
    .then(response => {
      this.setState({
        customerLoginData: response.data
      });
      if ( response.data.customerAccessTokenCreate.customerAccessToken ) {
      }
    });
  }
  

  render() {
    const { 
      closeCart, 
      openCart, 
      formatMoney, 
      handleUpdate, 
      handleSubscriptionUpdate, 
      createAccount, 
      customerLogin
    } = this;
    return (
      <>
        <SlideOutCart
          closeCart={closeCart}
          openCart={openCart}
          cartOpen={this.state.cartOpen}
          cart={this.state.cart}
          itemCount={this.state.itemCount}
          checkout={this.state.checkout}
          formatMoney={formatMoney}
          handleUpdate={handleUpdate}
        />
        <div className={ this.state.cartOpen === true ? "body-wrap toggled" : "body-wrap" } style={{marginLeft: this.state.cartOpen === true ? "-400px" : "0"}}>
          <Header 
            closeCart={closeCart} 
            itemCount={this.state.itemCount} 
            store={this.props.store}
            toggleRegionSelect={this.toggleRegionSelect}
            regionSelect={this.state.regionSelect}
          />
          <Main
            createAccount={createAccount}
            createAccountData={this.state.createAccountData}
            customerLogin={customerLogin}
            customerLoginData={this.state.customerLoginData}
            checkout={this.state.checkout}
            cart={this.state.cart}
            handleUpdate={handleUpdate}
            onSubscriptionUpdate={handleSubscriptionUpdate}
            products={this.state.products}
            itemCount={this.state.itemCount}
            formatMoney={formatMoney}
            openCart={openCart}
          />
          <Footer products={this.state.products} />  
        </div>
      </>
    );
  }
}

export default App;
